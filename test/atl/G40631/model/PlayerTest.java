/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import javafx.scene.paint.Color;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tuncer
 */
public class PlayerTest {

    /**
     * Test of getNom method, of class Player.
     */
    @Test
    public void testGetNom() {
        System.out.println("getNom");
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        String expResult = "Olaf";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCouleur method, of class Player.
     */
    @Test
    public void testGetCouleur() {
        System.out.println("getCouleur");
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        Color expResult = Color.CORAL;
        Color result = instance.getColor();
        assertEquals(expResult, result);

    }

    /**
     * Test of getScore method, of class Player.
     */
    @Test
    public void testGetScore() {
        //remise 2
    }

    /**
     * Test of getPièceRestant method, of class Player.
     */
    @Test
    public void testGetPièceRestant() {
        System.out.println("getPièceRestant");
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        Player player = new Player("Olaf", Color.RED,0, false);
        boolean[] expResult = player.getRemainingPiece();
        boolean[] result = instance.getRemainingPiece();
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of getPiece method, of class Player.
     */
    @Test
    public void testGetPiece() {
        System.out.println("getPiece");
        Player instance = new Player("Olaf", Color.CORAL,0, false);

        Piece[] expResult = instance.getPiece();
        Piece[] result = instance.getPiece();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of move method, of class Player.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testMove() throws PlateauSizeException {
        System.out.println("move");
        int x = 0;
        int y = 2;
        int pièce = 0;
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        Game game = new Game();
        game.start(20);
        boolean expResult = true;
        boolean result = instance.move(game, x, y, pièce);

        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class Player.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testMove1() throws PlateauSizeException {
        System.out.println("move1");
        int x = 0;
        int y = 0;
        int pièce = 0;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        boolean expResult = false;
        instance.move(game, x, y, pièce);
        boolean result = instance.move(game, x, y, pièce);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifTaille method, of class Player.
     */
    @Test
    public void testVerifTaille() {
        System.out.println("verifTaille");
        int pieces = 0;
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        boolean expResult = true;
        boolean result = instance.verifSize(pieces);
        assertEquals(expResult, result);

    }

    /**
     * Test of verifTaille method, of class Player.
     */
    @Test
    public void testVerifTaille1() {
        System.out.println("verifTaille1");
        int pieces = -5;
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        boolean expResult = false;
        boolean result = instance.verifSize(pieces);
        assertEquals(expResult, result);

    }

    /**
     * Test of verifTaille method, of class Player.
     */
    @Test
    public void testVerifTaille2() {
        System.out.println("verifTaille2");
        int pieces = 22;
        Player instance = new Player("Olaf", Color.CORAL,0, false);
        boolean expResult = false;
        boolean result = instance.verifSize(pieces);
        assertEquals(expResult, result);
    }
}
