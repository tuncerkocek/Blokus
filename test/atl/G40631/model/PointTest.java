/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tuncer
 */
public class PointTest {

    /**
     * Test of getX method, of class Point.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Point instance = new Point(0, 0);
        int expResult = 0;
        int result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Point.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Point instance = new Point(0, 2);
        int expResult = 2;
        int result = instance.getY();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Point.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Point p1 = new Point(0, 0);
        Point instance = new Point(0, 0);
        boolean expResult = true;
        boolean result = instance.equals(p1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Point.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals1");
        Point p1 = new Point(0, 1);
        Point instance = new Point(0, 0);
        boolean expResult = false;
        boolean result = instance.equals(p1);
        assertEquals(expResult, result);
    }
}
