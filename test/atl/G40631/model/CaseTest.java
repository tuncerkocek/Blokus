/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import javafx.scene.paint.Color;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tuncer
 */
public class CaseTest {

    public CaseTest() {
    }

    /**
     * Test of getCouleur method, of class Case.
     */
    @Test
    public void testGetCouleur() {
        System.out.println("getCouleur");
        Case instance = new Case(true, new Player("tuncer", Color.CORAL,0, false));
        Color expResult = Color.CORAL;
        Color result = instance.getCouleur();
        assertEquals(expResult, result);
    }

    /**
     * Test of isVide method, of class Case.
     */
    @Test
    public void testIsVide() {
        System.out.println("isVide");
        Case instance = new Case(false, new Player("", Color.CORAL,0, false));
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of isVide method, of class Case.
     */
    @Test
    public void testIsVide1() {
        System.out.println("isVide1");
        Case instance = new Case(false, new Player("", Color.CORAL,0, false));
        boolean expResult = false;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNom method, of class Case.
     */
    @Test
    public void testGetNom() {
        System.out.println("getNom");
        Case instance = new Case(false, new Player("", Color.CORAL,0, false));
        String expResult = "Reda";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

}
