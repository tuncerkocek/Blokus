/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import javafx.scene.paint.Color;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tuncer
 */
public class MoveGameTest {

    /**
     * Test of getPositionX method, of class MoveGame.
     */
    @Test
    public void testGetPositionX() {
        System.out.println("getPositionX");
        Player play = new Player("Olaf", Color.RED,0,false);
        MoveGame instance = new MoveGame(5, 0, 0, play);
        int expResult = 5;
        int result = instance.getPositionX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPositionY method, of class MoveGame.
     */
    @Test
    public void testGetPositionY() {
        System.out.println("getPositionY");
        Player play = new Player("Olaf", Color.RED,0,false);
        MoveGame instance = new MoveGame(5, 0, 0, play);
        int expResult = 5;
        int result = instance.getPositionX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPlayer method, of class MoveGame.
     */
    @Test
    public void testGetPlayer() {
        System.out.println("getPlayer");
        Player player = new Player("Olaf", Color.RED,0,false);
        MoveGame instance = new MoveGame(0, 0, 0, player);
        Player expResult = player;
        Player result = instance.getPlayer();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of firstMove method, of class MoveGame.
     */
    @Test
    public void testFirstMove() {

    }

    /**
     * Test of move method, of class MoveGame.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testMove() throws PlateauSizeException {
        System.out.println("move");
        Game game = new Game();
        game.start(20);
        Player player = new Player("Olaf", Color.RED,0,false);
        MoveGame instance = new MoveGame(5, 5, 5, player);
        boolean expResult = true;
        boolean result = instance.move(game);
        assertEquals(expResult, result);
    }

    /**
     * Test of enoughPlaceInBoard method, of class MoveGame.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testEnoughPlaceInBoard() throws PlateauSizeException {
        System.out.println("enoughPlaceInBoard");
        Piece piece = new Piece(0, 0, 1, "Top", "Top");
        Player player = new Player("Olaf", Color.RED,0,false);
        Game game = new Game();
        game.start(20);
        MoveGame instance = new MoveGame(19, 19, 3, player);
        boolean expResult = false;
        boolean result = instance.enoughPlaceInBoard(game, piece);
        assertEquals(expResult, result);
    }

    /**
     *
     * @throws PlateauSizeException
     */
    @Test
    public void testEnoughPlaceInBoard1() throws PlateauSizeException {
        System.out.println("enoughPlaceInBoard");
        Piece piece = new Piece(0, 0, 1);
        Player player = new Player("Olaf", Color.RED,0,false);
        Game game = new Game();
        game.start(20);
        MoveGame instance = new MoveGame(0, 0, 0, player);
        boolean expResult = true;
        boolean result = instance.enoughPlaceInBoard(game, piece);
        assertEquals(expResult, result);
    }

    /**
     * Test of regle method, of class MoveGame.
     */
    @Test
    public void testRegle() {
        //remise2
    }

}
