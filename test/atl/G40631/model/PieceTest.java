/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tuncer
 */
public class PieceTest {

    /**
     * Test of getForm method, of class Piece.
     */
    @Test
    public void testGetForm() {
        System.out.println("getForm");
        Piece instance = new Piece(0, 0, 1, "Top", "Top");
        Piece piece3 = new Piece(0, 0, 1, "Top", "Top");
        ArrayList<Point> expResult = piece3.getForm();
        ArrayList<Point> result = instance.getForm();
        assertEquals(expResult, result);
    }

}
