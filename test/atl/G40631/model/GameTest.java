/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tuncer
 */
public class GameTest {

    /**
     * Test of start method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test(expected = PlateauSizeException.class)
    public void testStart() throws PlateauSizeException {
        System.out.println("start");
        int plateau = 0;
        Game game = new Game();

        game.start(plateau);
    }

    /**
     * Test of getPlateauxSize method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGetPlateauxSize() throws PlateauSizeException {
        System.out.println("getPlateauxSize");
        int plateau = 40;
        Game game = new Game();

        game.start(plateau);
        int expResult = 40;
        int result = game.getBoardSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPlateaux method, of class Game.
     */
//    @Test
//    public void testGetPlateaux() throws PlateauSizeException {
//        System.out.println("getPlateaux");
//        int plateau = 40;
//        Game.start(plateau);
//        Case[][] expResult = new Case[plateau][plateau];
//        for (int i = 0; i < expResult.length; i++) {
//                for (int j = 0; j < expResult[0].length; j++) {
//                    expResult[i][j] = new Case(false, "", Color.TRANSPARENT);
//                }
//            }
//        Case[][] result = Game.getPlateaux();
//        assertArrayEquals(expResult, result);
//    }
    /**
     * Test of addPieceInGame method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testAddPieceInGame() throws PlateauSizeException {
        System.out.println("addPieceInGame");
        int x = 0;
        int y = 0;
        String nom = "Olaf";
        Color color = Color.RED;
        int plateau = 40;
        Game game = new Game();

        game.start(plateau);
        boolean expResult = true;
        assertEquals(expResult, game.addPieceInGame(x, y, nom, color));
    }

    /**
     * Test of equalsName method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testEqualsName() throws PlateauSizeException {
        System.out.println("equalsName");
        int x = 0;
        int y = 0;
        String nom = "Olaf";
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.equalsName(x, y, nom);
        assertEquals(expResult, result);
    }

    /**
     * Test of equalsName method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testEqualsName1() throws PlateauSizeException {
        System.out.println("equalsName");
        int x = 0;
        int y = 0;
        String nom = "Olaf";
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        game.addPieceInGame(x, y, nom, Color.RED);
        boolean expResult = true;
        boolean result = game.equalsName(x, y, nom);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifPlateaux method, of class Game.
     */
    @Test
    public void testVerifPlateaux() throws PlateauSizeException {
        System.out.println("verifPlateaux");
        int x = -1;
        int y = -1;
        boolean expResult = false;
        int plateau = 40;
        String nom = "Olaf";
        Game game = new Game();
        game.start(plateau);
        game.addPieceInGame(x, y, nom, Color.RED);
        boolean result = !game.verifBoard(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifPlateaux method, of class Game.
     */
    @Test
    public void testVerifPlateaux1() throws PlateauSizeException {
        System.out.println("verifPlateaux");
        int x = 4;
        int y = 4;
        int plateau = 40;
        String nom = "Olaf";
        Game game = new Game();
        game.start(plateau);
        game.addPieceInGame(x, y, nom, Color.RED);
        boolean expResult = true;
        boolean result = game.verifBoard(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifPosition method, of class Game.
     */
    @Test
    public void testVerifPosition() throws PlateauSizeException {
        System.out.println("verifPosition");
        int x = -15;
        int y = -15;
        int plateau = 40;

        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.verifPosition(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifPosition method, of class Game.
     */
    @Test
    public void testVerifPosition1() throws PlateauSizeException {
        System.out.println("verifPosition1");
        int x = 41;
        int y = 0;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.verifPosition(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifPosition method, of class Game.
     */
    @Test
    public void testVerifPosition2() throws PlateauSizeException {
        System.out.println("verifPosition2");
        int x = 15;
        int y = 0;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = true;
        boolean result = game.verifPosition(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifStock method, of class Game.
     */
    @Test
    public void testVerifStock() throws PlateauSizeException {
        System.out.println("verifStock");
        int pièce = 22;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.verifStock(pièce);
        assertEquals(expResult, result);
    }

    /**
     * Test of veriftock method, of class Game.
     */
    @Test
    public void testVerifStock1() throws PlateauSizeException {
        System.out.println("verifStock1");
        int pièce = -1;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.verifStock(pièce);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifStock method, of class Game.
     */
    @Test
    public void testVerifStock2() throws PlateauSizeException {
        System.out.println("verifStock2");
        int pièce = 0;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = true;
        boolean result = game.verifStock(pièce);
        assertEquals(expResult, result);
    }

    /**
     * Test of playTour method, of class Game.
     */
    @Test
    public void testPlayTour() throws PlateauSizeException {
        System.out.println("playTour");
        int pièce = 0;
        int positionX = -5;
        int positionY = 0;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.playTour(pièce, positionX, positionY);
        assertEquals(expResult, result);
    }

    /**
     * Test of playTour method, of class Game.
     */
    @Test
    public void testPlayTour2() throws PlateauSizeException {
        System.out.println("playTour2");
        int pièce = 0;
        int positionX = 0;
        int positionY = -5;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.playTour(pièce, positionX, positionY);
        assertEquals(expResult, result);

    }

    /**
     * Test of playTour method, of class Game.
     */
    @Test
    public void testPlayTour3() throws PlateauSizeException {
        System.out.println("playTour3");
        int pièce = -5;
        int positionX = 0;
        int positionY = -5;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.playTour(pièce, positionX, positionY);
        assertEquals(expResult, result);
    }

    /**
     * Test of playTour method, of class Game.
     */
    @Test
    public void testPlayTour4() throws PlateauSizeException {
        System.out.println("playTour4");
        int pièce = -5;
        int positionX = -5;
        int positionY = -5;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean expResult = false;
        boolean result = game.playTour(pièce, positionX, positionY);
        assertEquals(expResult, result);
    }

    /**
     * Test of playTour method, of class Game.
     */
    @Test
    public void testPlayTour5() throws PlateauSizeException {
        System.out.println("playTour5");
        int pièce = 1;
        int positionX = 1;
        int positionY = 1;
        Game game = new Game();
        game.start(40);
        boolean expResult = true;
        boolean result = game.playTour(pièce, positionX, positionY);
        assertEquals(expResult, result);
    }

    /**
     * Test of getStock method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGetStock() throws PlateauSizeException {
        System.out.println("getStock");
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        boolean[] expResult = new boolean[21];
        for (int i = 0; i < expResult.length; i++) {
            expResult[i] = true;
        }
        boolean[] result = game.getStock();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getNom method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGetNom() throws PlateauSizeException {
        System.out.println("getNom");
        String expResult = "Olaf";
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        String result = game.getNom();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPlayerName method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGetPlayerName() throws PlateauSizeException {
        System.out.println("getPlayerName");
        String expResult = "Blue";
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        String result = game.getPlayerName(1);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAllPiece method, of class Game.
     */
//    @Test
//    public void testGetAllPiece() throws PlateauSizeException {
//        System.out.println("getAllPiece");
//        int plateau = 40;
//        Game.start(plateau);
//        Player play = new Player("Olaf", Color.RED);
//        Piece[] result = Game.getAllPiece();
//        assertArrayEquals(play.getPiece(), result);
//    }
    /**
     * Test of getColor method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGetColor() throws PlateauSizeException {
        System.out.println("getColor");
        int number = 0;
        int plateau = 40;
        Game game = new Game();
        game.start(plateau);
        Player play = new Player("Olaf", Color.RED,0, false);
        Color expResult = play.getColor();
        Color result = game.getColor(number);
        assertEquals(expResult, result);
    }

    /**
     * Test of giveYOfAPoint method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGiveYOfAPoint() throws PlateauSizeException {
        System.out.println("giveYOfAPoint");
        Point p = new Point(0, 0);
        int expResult = 0;
        Game game = new Game();
        game.start(40);
        int result = game.giveYOfAPoint(p);
        assertEquals(expResult, result);
    }

    /**
     * Test of giveXOfAPoint method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGiveXOfAPoint() throws PlateauSizeException {
        System.out.println("giveXOfAPoint");
        Point p = new Point(0, 0);
        int expResult = 0;
        Game game = new Game();
        game.start(40);
        int result = game.giveXOfAPoint(p);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAllForm method, of class Game.
     *
     * @throws atl.G40631.model.PlateauSizeException
     */
    @Test
    public void testGetAllForm() throws PlateauSizeException {
        System.out.println("getAllForm");
        Piece piece2 = new Piece(0, 0, 2, "Top");
        Game game = new Game();
        game.start(40);
        ArrayList<Point> expResult = piece2.getForm();
        ArrayList<Point> result = game.getAllFormPlayer(piece2);
        assertEquals(expResult, result);
    }

}
