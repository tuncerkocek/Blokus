/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.observable;

/**
 *
 * @author tuncer
 */
public interface Observable {

    /**
     * Register an observer
     *
     * @param obs The observer to register
     */
    public void registerObserver(Observer obs);

    /**
     * Unregister an observer
     *
     * @param obs The observer to unregister
     */
    public void unregisterObserver(Observer obs);

    /**
     * Notify an observer
     */
    public void notifyObserver();

    /**
     * Notify an observer to erase his stroke
     *
     * @param numero
     */
    public void notifyObserver(int numero);

    /**
     * Notify all observer to unregister them
     *
     * @param name The name of the player whi want to stop to play
     */
    public void notifyObserver(String name);

}
