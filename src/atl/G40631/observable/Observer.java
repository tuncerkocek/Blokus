/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.observable;

import atl.G40631.model.Game;

/**
 *
 * @author tuncer
 */
public interface Observer {

    /**
     * Update
     *
     * @param obs
     */
    public void update(Observable obs);

    /**
     *
     * @param numero
     * @param playerName
     */
    public void update(int numero, String playerName);

    /**
     *
     * @param playerName
     * @param game
     */
    public void update(String playerName, Game game);

}
