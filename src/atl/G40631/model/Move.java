package atl.G40631.model;

/**
 *
 * @author G40631
 */
interface Move {

    boolean move(Game game);

    int getPositionX();

    int getPositionY();

    Player getPlayer();

    int getPièce();

    boolean enoughPlaceInBoard(Game game, Piece piece);
}
