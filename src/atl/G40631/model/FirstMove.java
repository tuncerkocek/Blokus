/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import java.util.Iterator;

/**
 *
 * @author tuncer
 */
public class FirstMove implements Move {

    private final int positionX;
    private final int positionY;
    private final int piece;
    private final Player player;

    /**
     * Constructor of a movement in the game
     *
     * @param x The position x to place the piece
     * @param y The position y to place the piece
     * @param pièce The number of piece choosed
     * @param player The player who made the movement
     */
    public FirstMove(int x, int y, int pièce, Player player) {
        positionX = x;
        positionY = y;
        piece = pièce;
        this.player = player;
    }

    /**
     * Give the position X of this movement
     *
     * @return the position X of this movement
     */
    @Override
    public int getPositionX() {
        return positionX;
    }

    /**
     * Give the y position of this movement
     *
     * @return the y position of this movement.
     */
    @Override
    public int getPositionY() {
        return positionY;
    }

    /**
     * Give the player who made this movement.
     *
     * @return the player who made this movement.
     */
    @Override
    public Player getPlayer() {
        Player playerEncapsulate = new Player(player.getName(), player.getColor(),player.getNumero(), player.isIa());
        return playerEncapsulate;
    }

    /**
     * Give the number of the piece placed with this movement
     *
     * @return the number of the piece placed with this movement
     */
    @Override
    public int getPièce() {
        return piece;
    }

    /**
     * Make a movement in the game with a law
     *
     * @return If the law is respected or not
     */
    private boolean rules(Piece piece, int joueur) {
        for (Point p : piece.getForm()) {
            int tempX = positionX + p.getX();
            int tempY = positionY + p.getY();
            if ((joueur == 0 && tempX == 0 && tempY == 0) || (joueur == 3 && tempX == 19 && tempY == 19) || (joueur == 1 && tempX == 19 && tempY == 0) || (joueur == 2 && tempX == 0 && tempY == 19)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Make a movement in the game
     *
     * @param game The current instance of the game
     * @return If the piece is place correctly or not
     */
    @Override
    public boolean move(Game game) {
        if (player.getRemainingPiece()[piece]) { // remise2
            if (player.verifSize(piece)) {
                Piece pièceRecupérer = player.getPiece()[piece];
                if (enoughPlaceInBoard(game, pièceRecupérer) && rules(pièceRecupérer, game.getCurrentPlayerNumber())) {
                    return pièceRecupérer.getForm().stream().noneMatch((p) -> (!game.addPieceInGame(positionX + p.getX(), positionY + p.getY(), player.getName(), player.getColor())));
                }
            }
        }
        return false;
    }

    /**
     * Verify if the place is available in the board for the piece to place
     *
     * @param game The current instance of the game
     * @param piece The piece to place in the board
     * @return If the place is available or not
     */
    @Override
    public boolean enoughPlaceInBoard(Game game, Piece piece) {
        for (Point p : piece.getForm()) {
            int x = positionX + p.getX();
            int y = positionY + p.getY();
            if (!game.verifBoard(x, y)) {
            } else {
                return false;
            }
        }
        return true;
    }

}
