/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

import javafx.scene.paint.Color;

/**
 *
 * @author Tuncer
 */
public class Case {

    private boolean empty = false;
    private Player player;

    /**
     * Create a new case in the board
     *
     * @param vide The availability of the case
     * @param player
     */
    public Case(boolean vide, Player player) {
        this.empty = vide;
        this.player = player;
    }

    /**
     * Give the color of the case
     *
     * @return The color of the case
     */
    public Color getCouleur() {
        return player.getColor();
    }

    /**
     * Tell if the case is empty or not
     *
     * @return true if the case is empty or false, in the other case
     */
    public boolean isEmpty() {
        return empty;
    }

    /**
     * Give the name of the player that have a piece in the selected case
     *
     * @return The name of the player that have a piece in the selected case
     */
    public String getName() {
        return this.player.getName();
    }

    /**
     * Put a piece in a case of the board with the name and color of the player
     *
     * @param vide The new availaibility of the case
     */
    void setCase(boolean vide, Player player) {
        this.empty = vide;
        this.player = player;
    }

}
