/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.model;

/**
 *
 * @author tuncer
 */
public class PlateauSizeException extends Exception {

    /**
     * Throw the eception if the number is greater than the board size of lesser
     * than 0
     *
     * @param nom The message to get
     */
    public PlateauSizeException(String nom) {
        super(nom);
    }
}
