package atl.G40631.model;

import java.util.ArrayList;
import javafx.scene.paint.Color;

/**
 *
 * @author G40631
 */
public class Player {

    private final String name;
    private final Color color;
    private final boolean[] remainingPiece;
    private final Piece piece[];
    private final ArrayList<Move> movedHistory;
    private int tour = 0;
    private final boolean ia;
    private final int number;

    int getNumero() {
        return number;
    }

    /**
     * Say if the player is an IA
     *
     * @return true if it's, or false, if it's not
     */
    public boolean isIa() {
        return ia;
    }

    /**
     * Create a new player of the Blokus game
     *
     * @param name The name of the new player
     * @param color The color of the new player
     * @param playerNumber The number of the player
     * @param ia If it's ia or not
     */
    public Player(String name, Color color, int playerNumber, boolean ia) {
        this.name = name;
        this.color = color;
        this.ia = ia;
        number = playerNumber;
        remainingPiece = new boolean[21];
        movedHistory = new ArrayList<>();
        for (int i = 0; i < remainingPiece.length; i++) {
            remainingPiece[i] = true;
        }
        piece = new Piece[21];
        createPiece();
    }


    /*
    *Create the 21 piece of the player for his stock.
     */
    private void createPiece() {
        Piece piece1 = new Piece(0, 0, 0);
        piece[0] = piece1;
        Piece piece2 = new Piece(0, 0, 1, "Top");
        piece[1] = piece2;
        Piece piece3 = new Piece(0, 0, 2, "Top", "Top");
        piece[2] = piece3;
        Piece piece4 = new Piece(0, 0, 3, "Left", "Down");
        piece[3] = piece4;
        Piece piece5 = new Piece(0, 0, 4, "Top", "Top", "Top");
        piece[4] = piece5;
        Piece piece6 = new Piece(0, 0, 5, "Right", "Top", "Top");
        piece[5] = piece6;
        Piece piece7 = new Piece(0, 0, 6, "Top", "Right", "Left", "Top");
        piece[6] = piece7;
        Piece piece8 = new Piece(0, 0, 7, "Down", "Right", "Top");
        piece[7] = piece8;
        Piece piece9 = new Piece(0, 0, 8, "Right", "Down", "Right");
        piece[8] = piece9;
        Piece piece10 = new Piece(0, 0, 9, "Top", "Top", "Top", "Top");
        piece[9] = piece10;
        Piece piece11 = new Piece(0, 0, 10, "Right", "Down", "Down", "Down");
        piece[10] = piece11;
        Piece piece12 = new Piece(0, 0, 11, "Top", "Right", "Top", "Top");
        piece[11] = piece12;
        Piece piece13 = new Piece(0, 0, 12, "Left", "Down", "Right", "Down");
        piece[12] = piece13;
        Piece piece14 = new Piece(0, 0, 13, "Right", "Down", "Down", "Right");
        piece[13] = piece14;
        Piece piece15 = new Piece(0, 0, 14, "Down", "Down", "Right", "Left", "Down");
        piece[14] = piece15;
        Piece piece16 = new Piece(0, 0, 15, "Right", "Right", "Left", "Down", "Down");
        piece[15] = piece16;
        Piece piece17 = new Piece(0, 0, 16, "Left", "Left", "Left", "Down", "Down");
        piece[16] = piece17;
        Piece piece18 = new Piece(0, 0, 17, "Right", "Top", "Right", "Top");
        piece[17] = piece18;
        Piece piece19 = new Piece(0, 0, 18, "Top", "Right", "Right", "Top");
        piece[18] = piece19;
        Piece piece20 = new Piece(0, 0, 19, "Top", "Right", "Right", "Left", "Top");
        piece[19] = piece20;
        Piece piece21 = new Piece(0, 1, 20, "Right", "Right", "Left", "Top", "Down", "Down");
        piece[20] = piece21;
    }

    /*
    *Give the name of this Player.
    *@return The name of this player
     */
    public String getName() {
        return name;
    }

    /*
    *Give his color
    *@return  The color of the player
     */
    public Color getColor() {
        return this.color;
    }

    /*
    *Give the stock of this player 
    *@return The stock of the player
     */
    public boolean[] getRemainingPiece() {
        boolean[] copyOfRemaining = new boolean[remainingPiece.length];
        System.arraycopy(remainingPiece, 0, copyOfRemaining, 0, remainingPiece.length);
        return copyOfRemaining;
    }

    /*
    *Give all piece of the player since the start.
    *@return All piece of the player.
     */
    public Piece[] getPiece() {
        Piece[] copyOfPiece = new Piece[piece.length];
        System.arraycopy(piece, 0, copyOfPiece, 0, piece.length);
        return piece;
    }

    /*
    *Make a move with a selected piece
    *@return if the piece is placed or not
     */
    public boolean move(Game game, int x, int y, int pièce) {
        Move move;
        if (tour == 0) {
            move = new FirstMove(x, y, pièce, this);

        } else {
            move = new MoveGame(x, y, pièce, this);
        }
        if (move.move(game)) {
            movedHistory.add(move);
            remainingPiece[pièce] = false;
            tour++;
            return true;
        }
        return false;
    }

    /*
    *Verify if the piece is between 0 and the size of the tableau of piece
    *@return if it's between or not.
     */
    public boolean verifSize(int pieces) {
        return pieces < piece.length && pieces >= 0;

    }

    int getTour() {
        return tour;
    }

}
