package atl.G40631.model;

/**
 *
 * @author g40631
 */
public class Point {

    private final int x;
    private final int y;

    /**
     * Constructor of a point
     *
     * @param x Coordinates X of a point
     * @param y Coordinates Y of a point
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /*
    * Return the coordinate X of this point
     */
    int getX() {
        return x;
    }

    /*
    * Return the coordinate Y of this point
     */
    int getY() {
        return y;
    }

    /**
     * Verify if the point give is the same
     *
     * @param obj The point to verify
     * @return true if it's equals, false, if it's not
     */
    @Override
    public boolean equals(Object obj) {
        Point p;
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        } else {
            p = (Point) obj;
        }

        return (this.x == p.getX() && this.y == p.getY());
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.x;
        hash = 71 * hash + this.y;
        return hash;
    }

}
