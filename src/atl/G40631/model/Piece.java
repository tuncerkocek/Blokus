package atl.G40631.model;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author G40631
 */
public class Piece {

    private ArrayList<Point> form;
    private int numberOfPiece;

    /**
     * The constructor of a piece of the blokus game
     *
     * @param x The start x position of the piece
     * @param y The start y positon of the piece
     * @param numero The number of the piece !
     * @param pos The direction to draw the piece
     */
    public Piece(int x, int y, int numero, String... pos) {
        form = new ArrayList<>();
        form.add(new Point(x, y));
        numberOfPiece = numero;
        for (String posi : pos) {
            switch (posi) {
                case "Top":
                    y++;
                    break;
                case "Down":
                    y--;
                    break;
                case "Left":
                    x--;
                    break;
                case "Right":
                    x++;
                    break;
                default:
                    break;
            }

            int index = 0;
            boolean out = true;
            while (index < form.size() && out) {
                Point p = form.get(index);
                if (p.equals(new Point(x, y))) {
                    out = false;
                }
                index++;
            }
            if (out) {
                form.add(new Point(x, y));
            }

        }

    }

    /**
     * The second constructor of a piece
     *
     * @param p The new list of position for the new piece
     * @param number The number of the new piece
     */
    public Piece(ArrayList<Point> p, int number) {
        form = p;
        sort();
        numberOfPiece = number;
    }

    private void sort() {
        int translateX = 0;
        int translateY = 0;
        for (Point p : form) {

            if (p.getX() < translateX) {
                translateX = p.getX();
            }
            if (p.getY() < translateY) {
                translateY = p.getY();
            }
        }
        for (int i = 0; i < form.size(); i++) {
            Point p = form.get(i);
            int x = p.getX() - translateX;
            int y = p.getY() - translateY;
            form.remove(i);
            form.add(i, new Point(x, y));
        }
        Collections.sort(form, (Point o1, Point o2) -> {
            int xComp = Integer.compare(o1.getX(), o2.getX());
            if (xComp == 0) {
                return Integer.compare(o1.getY(), o2.getY());
            } else {
                return xComp;
            }
        });

    }

    /**
     * Give the number of this piece
     *
     * @return The number of this piece
     */
    public int getNumberPiece() {
        return numberOfPiece;
    }

    /**
     * Give the all position that we need to draw the piece
     *
     * @return All position of the piece.
     */
    public ArrayList<Point> getForm() {

        if (form.get(0).getY() == 1) {
            ArrayList list = new ArrayList();
            form.forEach((p) -> {
                list.add(new Point(p.getX(), p.getY() - 1));
            });
            return list;
        } else {
            return form;
        }
    }

    /**
     * Give the form of this piece
     *
     * @return All point of this piece.
     */
    public ArrayList<Point> getFormPlayer() {
        return form;
    }

    /**
     *Return a piece 
     */
     void returned() {
        ArrayList<Point> listP = new ArrayList<>();
        this.getForm().forEach((p) -> {
            int x = Math.negateExact(p.getX());
            int y = p.getY();
            listP.add(new Point(x, y));
        });
        form = listP;
    }

    /**
     * Turn the piece by 45°
     */
    void turned() {
        ArrayList<Point> listP = new ArrayList<>();
        this.getForm().stream().map((p) -> {
            return p;
        }).forEachOrdered((p) -> {
            int x = p.getX();
            int y = p.getY();
            int temp = y * -1;
            y = x;
            x = temp;

            listP.add(new Point(x, y));
        });

        form = listP;
    }
}
