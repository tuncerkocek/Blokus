package atl.G40631.model;

/**
 *
 * @author G40631
 */
public class MoveGame implements Move {

    private final int positionX;
    private final int positionY;
    private final int piece;
    private final Player player;

    /**
     * Constructor of a movement in the game
     *
     * @param x The position x to place the piece
     * @param y The position y to place the piece
     * @param pièce The number of piece choosed
     * @param player The player who made the movement
     */
    public MoveGame(int x, int y, int pièce, Player player) {
        positionX = x;
        positionY = y;
        piece = pièce;
        this.player = player;
    }

    /**
     * Give the position X of this movement
     *
     * @return the position X of this movement
     */
    @Override
    public int getPositionX() {
        return positionX;
    }

    /**
     * Give the y position of this movement
     *
     * @return the y position of this movement.
     */
    @Override
    public int getPositionY() {
        return positionY;
    }

    /**
     * Give the player who made this movement.
     *
     * @return the player who made this movement.
     */
    @Override
    public Player getPlayer() {
        Player playerEncapsulate = new Player(player.getName(), player.getColor(), player.getNumero(), player.isIa());
        return playerEncapsulate;
    }

    /**
     * Give the number of the piece placed with this movement
     *
     * @return the number of the piece placed with this movement
     */
    @Override
    public int getPièce() {
        return piece;
    }

    /**
     * Make a movement in the game
     *
     * @param game The current instance of the game
     * @return If the piece is place correctly or not
     */
    @Override
    public boolean move(Game game) {
        if (player.getRemainingPiece()[piece]) {
            if (player.verifSize(piece)) {
                Piece pièceRecupérer = player.getPiece()[piece];
                if (enoughPlaceInBoard(game, pièceRecupérer) && rules(pièceRecupérer, game)) {
                    pièceRecupérer.getForm().forEach((p) -> {
                        game.addPieceInGame(positionX + p.getX(), positionY + p.getY(), player.getName(), player.getColor());
                    });
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verify if the place is available in the board for the piece to place
     *
     * @param game The current instance of the game
     * @param piece The piece to place in the board
     * @return If the place is available or not
     */
    @Override
    public boolean enoughPlaceInBoard(Game game, Piece piece) {
        for (Point p : piece.getForm()) {
            int x = positionX + p.getX();
            int y = positionY + p.getY();

            if (!game.verifBoard(x, y)) {

            } else {
                return false;
            }
        }
        return true;
    }

    boolean rules(Piece piece, Game game) {
        int i = 0;
        boolean finded = false;
        while (i < piece.getForm().size()) {
            Point p = piece.getForm().get(i);
            int tempX = positionX + p.getX();
            int tempY = positionY + p.getY();

            if (right(tempX, tempY, game) || left(tempX, tempY, game) || top(tempX, tempY, game) || down(tempX, tempY, game)) {
                return false;
            } else if (!diagonaleLeftDown(tempX, tempY, game) && !finded) {
                if (!diagonaleLeftTop(tempX, tempY, game)) {
                    if (!diagonaleRightDown(tempX, tempY, game)) {
                        if (!diagonaleRightTop(tempX, tempY, game)) {

                        } else {
                            finded = true;
                        }
                    } else {

                        finded = true;
                    }
                } else {

                    finded = true;
                }

            } else {

                finded = true;
            }
            i++;

        }

        return finded;
    }

    private boolean right(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            x++;

            if (x < 0 || x == game.getBoardSize() || y < 0 || y == game.getBoardSize()) {
                return false;
            } else {
                return game.equalsName(x, y, player.getName());
            }
        }
        return false;
    }

    private boolean left(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            x--;

            if (x < 0 || x == game.getBoardSize() || y < 0 || y == game.getBoardSize()) {
                return false;
            } else {
                return game.equalsName(x, y, player.getName());
            }
        }
        return false;
    }

    private boolean top(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            y++;
            if (x < 0 || x == game.getBoardSize() || y < 0 || y == game.getBoardSize()) {
                return false;
            } else {
                return game.equalsName(x, y, player.getName());
            }
        }
        return false;
    }

    private boolean down(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            y--;

            if (x < 0 || x == game.getBoardSize() || y < 0 || y == game.getBoardSize()) {
                return false;
            } else {
                return game.equalsName(x, y, player.getName());
            }
        }
        return false;
    }

    private boolean diagonaleLeftTop(int x, int y, Game game) {
        if (!verifPosition(game, x, y)) {
            x--;
            y--;

            return game.equalsName(x, y, player.getName());
        }
        return false;
    }

    private boolean diagonaleLeftDown(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            x--;
            y++;

            return game.equalsName(x, y, player.getName());
        }
        return false;
    }

    private boolean diagonaleRightDown(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            x++;
            y++;

            return game.equalsName(x, y, player.getName());
        }
        return false;
    }

    private boolean diagonaleRightTop(int x, int y, Game game) {

        if (!verifPosition(game, x, y)) {
            x++;
            y--;

            return game.equalsName(x, y, player.getName());
        }
        return false;
    }

    /**
     * Verify if the x and y is between the board size
     *
     * @param x The x position to verify
     * @param y The y position to verify
     * @return If it's between or not
     */
    private boolean verifPosition(Game game, int x, int y) {
        return game.verifBoard(x, y);
    }
}
