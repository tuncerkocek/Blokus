package atl.G40631.model;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.paint.Color;

import atl.G40631.observable.Observable;
import atl.G40631.observable.Observer;
import java.util.List;

/**
 *
 * @author G40631
 */
public class Game implements Observable {

    private int currentPlayer = 0;
    private Piece currentChoosedPiece;
    private ArrayList<Player> totalePlayerList;
    private double mouseX;
    private double mouseY;
    private final List<Observer> observers;
    private Case[][] board;
    private int sizeOfBoard;
    private List<Integer> dispo;

    /**
     * Start a new instance of game !
     */
    public Game() {
        this.observers = new ArrayList();
    }

    /**
     * If the player is a ia
     *
     * @return true if it's, not if it's not
     */
    public boolean iaPlay() {

        if (!totalePlayerList.isEmpty() && totalePlayerList.get(currentPlayer).isIa()) {
            int pièce = (int) Math.floor(Math.random() * 20) + 0;
            boolean pieceExistant = false;
            int test = 0;
            dispo.clear();
            while (test < 21) {
                if (giveRemainingPiece()[test]) {
                    dispo.add(test);
                }
                test++;
            }
            if (dispo.size() > 0) {
                int compteur = 0;

                while (!giveRemainingPiece()[pièce] && compteur < 50) {

                    pièce = (int) Math.floor(Math.random() * 20) + 0;
                    compteur++;
                }
                if (compteur >= 50) {
                    if (test == 21) {
                        test--;
                    }
                    pièce = test;
                }

                choosedAPieceIA(getNom(), pièce);

                int x = -1;
                int y = -1;
                boolean joue = false;
                while (!dispo.isEmpty() && !joue) {
                    while (x < board.length && !joue) {
                        y = -1;
                        x++;
                        while (y < board[0].length && !joue) {
                            y++;
                            int k = 0;
                            while (k < 4 && !joue) {
                                int j = 0;
                                while (j < 2 && !joue) {
                                    joue = playTour(pièce, x, y);
                                    if (!joue) {
                                        currentChoosedPiece.returned();
                                        getAllPiece()[currentChoosedPiece.getNumberPiece()].returned();
                                    }
                                    j++;
                                }
                                if (!joue) {
                                    currentChoosedPiece.turned();
                                    getAllPiece()[currentChoosedPiece.getNumberPiece()].turned();
                                }
                                k++;
                            }

                        }

                    }
                    if (joue) {
                        mouseX = x;
                        mouseY = y;
                        notifyObserver("Add");

                        return true;
                    }
                    pièce = dispo.remove(0);
                }
            }
        }

        return false;
    }

    /**
     * Add in board the selected piece
     *
     * @param x The x position where to put piece
     * @param y The y position where to put piece
     * @return
     */
    public boolean addBoard(int x, int y) {
        mouseX = x;
        mouseY = y;
        if (currentChoosedPiece != null) {
            int sommeX = (int) x + currentChoosedPiece.getForm().get(0).getX();
            int sommeY = (int) y + currentChoosedPiece.getForm().get(0).getY();
            if (playTour(currentChoosedPiece.getNumberPiece(), sommeX, sommeY)) {
                notifyObserver("Add");
                nextTour(false);
                return true;
            }
        }
        return false;
    }

    /**
     * Make a next tour.
     *
     * @param remove if the current Player stop to play
     */
    public void nextTour(boolean remove) {
        if (remove) {
            removeUser();
            if (currentPlayer >= totalePlayerList.size()) {
                currentPlayer = 0;
            }
        } else {
            next();
        }
        while (!totalePlayerList.isEmpty() && isIa()) {

            if (!iaPlay() && totalePlayerList.get(currentPlayer).getTour() > 1) {
                removeUser();
                if (currentPlayer >= totalePlayerList.size()) {
                    currentPlayer = 0;
                }
            } else {
                next();
            }

        }

    }

    /**
     * Make a new tour
     */
    public void nexTourIA() {
        while (!totalePlayerList.isEmpty() && isIa()) {
            if (!iaPlay() && totalePlayerList.get(currentPlayer).getTour() > 1) {
                removeUser();
                if (currentPlayer >= totalePlayerList.size()) {
                    currentPlayer = 0;
                }
            }
            next();
        }
    }

    /**
     * Select a piece on the userside !
     *
     * @param name The current player name
     * @param number The selected piece number
     */
    public void chooseAPiece(String name, int number) {
        if (getNom() != null && getNom().equals(name)) {
            currentChoosedPiece = getAllPiece()[number];
            this.notifyObserver(number);
        }
    }

    /**
     * Select a piece for the ia
     *
     * @param name The name of the ia player
     * @param number The number of the piece selected
     */
    public void choosedAPieceIA(String name, int number) {
        if (getNom() != null && getNom().equals(name)) {
            currentChoosedPiece = getAllPiece()[number];
        }
    }

    /*
    * Search if the player name is the same of the case on board
    *@param x the position x of a case that we need to compare the name
    *@param y the position y of a case that we need to compare the name
    *@param nom the name of the player that we need to compare
     */
    /**
     * Look if the player has a piece in a case!
     *
     * @param x The position x where we want to look
     * @param y The position y of the case we want to look
     * @param name The name of the player
     * @return If the player has a piece !
     */
    public boolean equalsName(int x, int y, String name) {
        if (verifSize(x, y)) {
            if (board[x][y].getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Give all the position of the piece
     *
     * @param piec The piece that we need all position
     * @return All position of the passed piece
     */
    public ArrayList<Point> getAllFormPlayer(Piece piec) {
        return piec.getFormPlayer();
    }

    /**
     * Give all piece of the current Player
     *
     * @return The allPiece of the currentPlayer
     */
    public Piece[] getAllPiece() {
        return totalePlayerList.get(currentPlayer).getPiece();
    }

    /**
     * Give the color of the selected player
     *
     * @param number The number of a player
     * @return The color of the selected player
     */
    public Color getColor(int number) {
        return totalePlayerList.get(number).getColor();
    }

    /**
     * Give the color of the current player
     *
     * @return The color of the current player
     */
    public Color getColorCurrentPlayer() {
        return totalePlayerList.get(currentPlayer).getColor();
    }

    /**
     * Give the number of the current player
     *
     * @return The current player's number
     */
    public int getCurrentPlayerNumber() {
        return totalePlayerList.get(currentPlayer).getNumero();
    }

    /**
     * Give the current choosed piece
     *
     * @return The current choosed piece
     */
    public Piece getHistorique() {
        return currentChoosedPiece;
    }

    /**
     * Give the x position where the mouse is.
     *
     * @return The x position of the mouse.
     */
    public double getMouseX() {
        return mouseX;
    }

    /**
     * Give the y position where mouse is.
     *
     * @return The y position of the mouse
     */
    public double getMouseY() {
        return mouseY;
    }

    /**
     * Give the name of the current player
     *
     * @return The name of the current player
     */
    public String getNom() {
        return totalePlayerList.get(currentPlayer).getName();
    }

    /**
     * Give the remaining piece tab of the current user.
     *
     * @return Give his remaining piece tab.
     */
    public boolean[] giveRemainingPiece() {
        return totalePlayerList.get(currentPlayer).getRemainingPiece();
    }

    /**
     * Give the board
     *
     * @return The board.
     */
    public Case[][] getBoard() {
        return Arrays.copyOf(board, sizeOfBoard);
    }

    /**
     * Give the size of the board.
     *
     * @return the board size
     */
    public int getBoardSize() {
        return board.length;
    }

    /**
     * Give the asked player Name
     *
     * @param i The number of player
     * @return The name of the asked player
     */
    public String getPlayerName(int i) {
        return totalePlayerList.get(i).getName();
    }

    /**
     * Give the stock of the current player
     *
     * @return The stock of the current player
     */
    public boolean[] getStock() {
        return totalePlayerList.get(currentPlayer).getRemainingPiece();
    }

    /**
     * Give the player stock
     *
     * @param i The number of the player that we want to receive the stock
     * @return The stock of the asked player
     */
    public boolean[] getStockPlayer(int i) {
        return totalePlayerList.get(i).getRemainingPiece();
    }

    /**
     * Give the stock of a player by the name
     *
     * @param name The name of the player that we want his stock
     * @return the remaing stock of the player.
     */
    public boolean[] getStockPlayer(String name) {
        int i = 0;
        boolean findName = false;
        while (i < totalePlayerList.size() && !findName) {
            findName = totalePlayerList.get(i).getName().equals(name);
            if (findName) {
                return totalePlayerList.get(i).getRemainingPiece();
            }
            i++;
        }
        return new boolean[0];
    }

    /**
     * Give the x position of the Point
     *
     * @param p The point that we want the x position
     * @return The x position of the point
     */
    public int giveXOfAPoint(Point p) {
        return p.getX();
    }

    /**
     * Give the y position of the Point
     *
     * @param p The point that we want the y position
     * @return The y position of the point
     */
    public int giveYOfAPoint(Point p) {
        return p.getY();
    }

    /**
     * Say if the player is a IA or not
     *
     * @return true if it's or false, if not.
     */
    public boolean isIa() {
        if (!totalePlayerList.isEmpty()) {
            return totalePlayerList.get(currentPlayer).isIa();
        }
        return false;

    }

    /**
     * verify if the player is a IA or not
     *
     * @param i The player number that we want to verify
     * @return True if it's or false, if not
     */
    public boolean isIa(int i) {
        return totalePlayerList.get(i).isIa();
    }

    /**
     * Create a new Game
     *
     * @throws PlateauSizeException If the plateau size is under 0
     */
    public void newGame() throws PlateauSizeException {
        int taillePlateau = 20;
        this.start(taillePlateau);
        notifyObserver("New");

    }

    /**
     * increment the currentPlayer
     */
    public void next() {
        currentPlayer++;
        if (currentPlayer >= totalePlayerList.size()) {
            currentPlayer = 0;
        }
        currentChoosedPiece = null;
    }

    /**
     * Notify the observer to unregister all placed piece
     */
    @Override
    public void notifyObserver() {
        observers.forEach((obs) -> {
            obs.update(this);
        });
    }

    /**
     * Notify an observer to erase his stroke
     *
     * @param numero The number of piece !
     */
    @Override
    public void notifyObserver(int numero) {
        observers.forEach((obs) -> {
            obs.update(numero, getNom());
        });
    }

    /**
     * Notify all observer to unregister them
     *
     * @param name The name of the player whi want to stop to play
     */
    @Override
    public void notifyObserver(String name) {
        observers.forEach((obs) -> {
            obs.update(name, this);
        });
    }

    /**
     * Place a piece to the board and change player if the piece is placed.
     *
     * @param pièce The piece choosed by the current player to add
     * @param positionX The position x where to place the selected piece
     * @param positionY The position y where to place the selected piece
     * @return If the place is place or not.
     */
    public boolean playTour(int pièce, int positionX, int positionY) {
        return this.verifSize(positionX, positionY) && totalePlayerList.get(currentPlayer).move(this, positionX, positionY, pièce);
    }

    /**
     * Give the total player
     *
     * @return The number of player that play the game
     */
    public int playerSize() {
        return totalePlayerList.size();
    }

    /**
     * Register an observer
     *
     * @param obs The observer to register
     */
    @Override
    public void registerObserver(Observer obs) {
        observers.add(obs);
    }

    /**
     * Remove a user in the game!
     *
     */
    public void removeUser() {
        String nom = getNom();
        totalePlayerList.remove(totalePlayerList.get(currentPlayer));
        if (totalePlayerList.isEmpty()) {
            notifyObserver("Alert");
        } else {
            setCurrentChoosedPieceNull();
            notifyObserver(nom);
        }
    }

    /**
     * return a piece
     */
    public void retournerPiece() {
        currentChoosedPiece.returned();
    }

    /**
     * Put null in the current choosed piece
     */
    public void setCurrentChoosedPieceNull() {
        this.currentChoosedPiece = null;
    }

    /**
     * Start a new blokus party
     *
     * @param plateau the size of the new board
     * @throws atl.G40631.model.PlateauSizeException
     */
    public void start(int plateau) throws PlateauSizeException {
        sizeOfBoard = plateau;
        if (plateau > 0) {
            currentPlayer = 0;

            board = new Case[plateau][plateau];
            for (Case[] plateaux1 : board) {
                for (int j = 0; j < board[0].length; j++) {
                    plateaux1[j] = new Case(false, new Player("", Color.TRANSPARENT, -1, false));
                }
            }
            dispo = new ArrayList<>();
            totalePlayerList = new ArrayList<>();
            Player player = new Player("Olaf", Color.RED, 0, true);
            Player player1 = new Player("Blue", Color.BLUE, 1, true);
            Player player2 = new Player("Francois", Color.GREEN, 2, false);
            Player player3 = new Player("Luka", Color.YELLOW, 3, false);
            totalePlayerList.addAll(Arrays.asList(player, player1, player2, player3));

        } else {
            throw new PlateauSizeException("Taille du plateau incorrect");
        }
    }

    /**
     * Turn a piece by 45° at any time
     */
    public void tournerPiece() {
        currentChoosedPiece.turned();
    }

    /**
     * Unregister an observer
     *
     * @param obs The observer to unregister
     */
    @Override
    public void unregisterObserver(Observer obs) {
        if (observers.contains(obs)) {
            observers.remove(obs);
        }
    }

    /**
     * Verify if x and y is in the between 0 and the size of the board
     *
     * @param x The position x in board to verify
     * @param y The position y in board to verify
     * @return if the position is correct or not.
     */
    public boolean verifPosition(int x, int y) {
        return !(x < 0 || x >= board.length || y < 0 || y >= board[x].length);

    }

    /**
     * Verify if the piece choose is still in stock of the current player
     *
     * @param pièce The piece choosed by the current player
     * @return If it's still in his stock or not
     */
    public boolean verifStock(int pièce) {
        return !(pièce < 0 || pièce > totalePlayerList.get(currentPlayer).getRemainingPiece().length
                || !totalePlayerList.get(currentPlayer).getRemainingPiece()[pièce]);
    }

    /**
     * Make a visual shape at the view
     *
     * @param mouseX The position x where the mouse is
     * @param mouseY The position y where the mouse is
     */
    public void visual(int mouseX, int mouseY) {
        if (currentChoosedPiece != null) {
            this.mouseX = mouseX;
            this.mouseY = mouseY;
            notifyObserver("Visual");
        }
    }

    /*
    * Verify if the x and y is in the interval of the board size
    *@param x the position x of a case that we need to verify
    *@param y the position y of a case that we need to verify
     */
    private boolean verifSize(int x, int y) {
        return x >= 0 && x < board.length && y >= 0 && y < board[x].length;
    }

    /*
    * Add piece to the board
    *@param x the position x of a piece to add in the board
    *@param y the position y of a piece to add in the board
     */
    boolean addPieceInGame(int x, int y, String nom, Color color) {
        x = Math.abs(x);

        if (verifSize(x, y)) {
            board[x][y].setCase(true, totalePlayerList.get(currentPlayer));
            return true;
        }
        return false;
    }

    /*
    * Verify if a case is empty or not
    *@param x the position x of a case that we need to verify if it's empty or not
    *@param y the position y of a case that we need to verify if it's empty or not
     */
    boolean verifBoard(int x, int y) {
        if (verifSize(x, y)) {
            return board[x][y].isEmpty();
        } else {
            return true;
        }
    }
}
