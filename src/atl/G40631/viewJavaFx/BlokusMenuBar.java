/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;

/**
 *
 * @author tuncer
 */
public class BlokusMenuBar extends MenuBar {

    private final Menu menu1 = new Menu("File");
    private final Menu menu2 = new Menu("Options");
    private final Menu menu3 = new Menu("Help");

    /**
     * Create a menu bar for blokus
     */
    public BlokusMenuBar() {
        this.getMenus().addAll(menu1, menu2, menu3);
    }

}
