/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import atl.G40631.model.PlateauSizeException;
import atl.G40631.observable.Observable;
import atl.G40631.observable.Observer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Tuncer
 */
public class Blokus extends Application implements Observer {

    private Stage primaryStage;
    private Game game;

    @Override
    public void start(Stage primaryStage) throws PlateauSizeException {
        this.primaryStage = primaryStage;

        game = new Game();
        game.start(20);
        game.registerObserver(this);
        create(game);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void update(Observable obs) {
    }

    @Override
    public void update(int numero, String playerName) {
    }

    @Override
    public void update(String playerName, Game game) {
        if (playerName.equals("New")) {
            create(game);
        }
    }

    private void create(Game game) {
        BlokusBorderPane root = new BlokusBorderPane(game);
        Scene scene = new Scene(root);
        root.setStyle("-fx-background-color: white;");
        primaryStage.setTitle("Blokus");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
