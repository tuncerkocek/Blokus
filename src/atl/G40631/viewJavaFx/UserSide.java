/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

/**
 *
 * @author Tuncer
 */
public class UserSide extends VBox {

    /*
    *Create the all piece and add it to the leftSide of the view.
    *@param vbox The vbox where to place element
    @param i The number of the player for who we create piece.
     */
    /**
     * Create an vbox that will contains all users
     *
     * @param game The current instance of game
     */
    public UserSide(Game game) {

        this.setAlignment(Pos.BASELINE_LEFT);

        for (int j = 0; j < game.playerSize(); j++) {
            UserContainer verticalBox = new UserContainer(game, j);
            this.getChildren().add(verticalBox);
        }

    }

}
