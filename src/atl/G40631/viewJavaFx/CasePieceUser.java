/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import atl.G40631.observable.Observable;
import atl.G40631.observable.Observer;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 *
 * @author tuncer
 */
public class CasePieceUser extends GridPane implements Observer {

    private final int number;
    private final String playerName;
    private boolean taked;
    private final EventHandler event;

    /**
     * Create a piece for a user
     *
     * @param i The number of the piece
     * @param game The current instance of the game
     * @param numberOfPlayer The number of the player
     */
    public CasePieceUser(int i, Game game, int numberOfPlayer) {
        if (!game.isIa(numberOfPlayer)) {
            game.registerObserver(this);
        }
        number = i;
        taked = false;
        playerName = game.getPlayerName(numberOfPlayer);
        this.setPadding(new Insets(2, 2, 2, 2));
        this.event = (EventHandler) (Event t) -> {
            if (!taked) {
                CasePieceUser pane = (CasePieceUser) t.getSource();
                game.chooseAPiece(pane.getPlayerName(), pane.getNumero());
            }
        };

        this.addEventHandler(MouseEvent.MOUSE_CLICKED, event
        );

    }

    /**
     * Give the number of the piece
     *
     * @return The number of the piece
     */
    public int getNumero() {
        return number;
    }

    /**
     *
     * @param obs
     */
    @Override
    public void update(Observable obs) {

    }

    /**
     *
     * @param numero
     * @param playerName
     */
    @Override
    public void update(int numero, String playerName) {
        unStroke();
        if (this.number == numero && this.playerName.equals(playerName)) {
            this.setStyle("-fx-border-width:1px;-fx-border-color:grey;");
            taked = true;
        } else {
            taked = false;
        }

    }

    private void unStroke() {
        this.setStyle("-fx-border-width:1px;-fx-border-color:none;");
    }

    /**
     * Give the piece owner's name
     *
     * @return Give the player name
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     *
     * @param playerName
     */
    @Override
    public void update(String playerName, Game game) {

        if (playerName.equals("Add") && this.playerName.equals(game.getNom())) {
            if (taked) {
                this.removeEventHandler(MouseEvent.MOUSE_CLICKED, event);
                unStroke();
            }
        }
    }
}
