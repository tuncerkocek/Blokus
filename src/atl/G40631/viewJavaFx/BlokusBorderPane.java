/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;
import atl.G40631.model.Game;
import atl.G40631.observable.Observable;
import atl.G40631.observable.Observer;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author tuncer
 */
public class BlokusBorderPane extends BorderPane implements Observer {

    /**
     * Create à borderpane
     *
     * @param game The current instance of the game!
     */
    public BlokusBorderPane(Game game) {
        game.registerObserver(this);
        BlokusMenuBar menuBar = new BlokusMenuBar();
        this.setTop(menuBar);
        UserSide leftSide = new UserSide(game);
        this.setLeft(leftSide);
        Board right = new Board(game, game.getBoardSize());
        this.setRight(right);
        ButtonContainer down = new ButtonContainer(game, right);
        this.setBottom(down);
    }

    /**
     *
     * @param obs
     */
    @Override
    public void update(Observable obs) {

    }

    /**
     *
     * @param numero
     * @param playerName
     */
    @Override
    public void update(int numero, String playerName) {

    }

    /**
     *
     * @param playerName
     */
    @Override
    public void update(String playerName, Game game) {
    }

}
