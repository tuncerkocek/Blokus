/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.scene.control.Button;

/**
 *
 * @author tuncer
 */
public class Turn extends Button {

    /**
     * Create a rotate piece button
     *
     * @param text The text of the button
     * @param game The current instance of the game
     */
    public Turn(String text, Game game) {
        super(text);
        this.setOnAction((event) -> {
            game.tournerPiece();
        });
    }

}
