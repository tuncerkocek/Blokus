/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

/**
 *
 * @author tuncer
 */
public class Pass extends Button {

    /**
     * Create a new pass my tour button
     *
     * @param text The text of the button
     * @param game The current instance of the game
     * @param plateau The current instance of the plateaux
     */
    public Pass(String text, Game game, Board plateau) {
        super(text);
        this.setOnAction((ActionEvent event) -> {
            game.nextTour(false);
        });
    }

}
