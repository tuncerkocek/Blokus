/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import atl.G40631.model.Piece;
import atl.G40631.model.Point;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

/**
 *
 * @author tuncer
 */
public class LinePiece extends HBox {

    /**
     * Create an hbox that contains all of piece
     *
     * @param game The current instance of the game
     * @param player The number of the player
     * @param departure The begin number where to stop to take in piece
     * @param end The end number where to stop to take in piece
     */
    public LinePiece(Game game, int player, int departure, int end) {
        this.setPadding(new Insets(5, 5, 5, 5));
        drawAllPiece(game, player, departure, end);
    }

    private void drawAllPiece(Game game, int numberOfPlayer, int departure, int end) {
        int index = 0;
        int negation;
        Piece[] tabPiece = game.getAllPiece();
        for (int i = departure; i < end && i < tabPiece.length; i++) {
            Piece piec = tabPiece[i];
            CasePieceUser pane = new CasePieceUser(i, game, numberOfPlayer);
            for (int x = 0; x < 4; x++) {
                negation = 4;
                for (int y = 0; y < 4; y++) {
                    for (Point point : game.getAllFormPlayer(piec)) {
                        addInPane(game, pane, point, x, y, negation, numberOfPlayer);
                    }
                    negation = negation - 2;
                }
            }
            this.getChildren().add(pane);
            index++;
        }
    }

    private void addInPane(Game game, GridPane pane, Point point, int x, int y, int negation, int numberOfPlayer) {
        MyRectangle rect = new MyRectangle(12, 12, x, y, game);
        rect.setStroke(Color.BLACK);
        if (x == Math.abs(game.giveXOfAPoint(point)) && y == Math.abs(game.giveYOfAPoint(point))) {

            rect.fillPiece(game.getColor(numberOfPlayer));

        } else {
            rect.empty();
        }
        pane.getChildren().add(rect);
        int ne = y + negation;
        GridPane.setConstraints(rect, x, ne);
    }

}
