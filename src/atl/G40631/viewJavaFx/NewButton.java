/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import atl.G40631.model.PlateauSizeException;

import javafx.scene.control.Button;

/**
 *
 * @author tuncer
 */
public class NewButton extends Button{

    /**
     * Create a new party button
     *
     * @param text The text of the new button
     * @param game The current instance of game
     */
    public NewButton(String text, Game game) {
        super(text);
        this.setOnAction((event) -> {
            try {
                game.newGame();
            } catch (PlateauSizeException ex) {
            }
        });
    }



}
