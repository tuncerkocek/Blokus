/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

/**
 *
 * @author tuncer
 */
public class ButtonContainer extends HBox {

    /**
     * Create a button container
     *
     * @param game The current instance of the game
     * @param board The current instance of the board
     */
    public ButtonContainer(Game game, Board board) {
        NewButton newGame = new NewButton("Nouvelle partie", game);
        Pass pass = new Pass("Je passe", game, board);
        Stop arreter = new Stop("J'arrête", game, board);
        Turn tourner = new Turn("Tourner", game);
        this.getChildren().addAll(newGame, pass, arreter, tourner);
        this.setSpacing(10);
        this.setAlignment(Pos.CENTER);
    }

}
