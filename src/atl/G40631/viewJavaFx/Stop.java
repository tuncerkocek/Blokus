/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.scene.control.Button;

/**
 *
 * @author tuncer
 */
public class Stop extends Button {

    /**
     * Create a button
     *
     * @param text The text of the button
     * @param game The current instance of the game
     * @param plateau The current instance of the board
     */
    public Stop(String text, Game game, Board plateau) {
        super(text);
        
        this.setOnAction((event) -> {
            game.nextTour(true);
        });
    }

}
