/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import atl.G40631.observable.Observable;
import atl.G40631.observable.Observer;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 *
 * @author tuncer
 */
public class HBoxPlayer extends HBox implements Observer {

    /**
     * Give the name of the player
     *
     * @return The name of the player
     */
    public String getName() {
        return name.getText();
    }

    private final Label name;
    private final Label numberScore;
    private final Label piece;

    /**
     * Create an hbox with the name,score... of the player
     *
     * @param game The current instance of the game
     * @param j The number of player !
     */
    public HBoxPlayer(Game game, int j) {
        game.registerObserver(this);
        name = new Label(game.getPlayerName(j));
        name.setPadding(new Insets(2, 40, 0, 10));
        Label score = new Label("Score");
        score.setPadding(new Insets(2, 40, 0, 0));
        numberScore = new Label("0");
        numberScore.setPadding(new Insets(2, 40, 0, 0));
        Label oke = new Label("Pièce restant");
        oke.setPadding(new Insets(2, 40, 0, 0));
        piece = new Label(String.valueOf(game.getStockPlayer(j).length));
        piece.setPadding(new Insets(2, 40, 0, 0));
        this.getChildren().addAll(name, score, numberScore, oke, piece);
        this.setSpacing(15);
    }

    @Override
    public void update(Observable obs) {

    }

    @Override
    public void update(int numero, String playerName) {

    }

    @Override
    public void update(String playerName, Game game) {

        int score = 0;

        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoard()[0].length; j++) {
                if (game.getBoard()[i][j].getName().equals(name.getText())) {
                    score++;
                }
            }
        }

        numberScore.setText(String.valueOf(score));
        int remainingPiece = 0;
        for (boolean reste : game.getStockPlayer(name.getText())) {
            if (reste) {
                remainingPiece++;
            }

            piece.setText(String.valueOf(remainingPiece));
        }
    }
}
