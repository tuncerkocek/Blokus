/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author tuncer
 */
public class MyRectangle extends Rectangle {

    private Color color;
    private final Color stroke;
    private Color lastColor;
    private final int x;
    private final int y;

    /**
     * Give the position x of the rectangle
     *
     * @return the position x of the rectangle
     */
    public int getXX() {
        return x;
    }

    /**
     * Give the position y of the rectangle
     *
     * @return The position y of the rectangle
     */
    public int getYY() {
        return y;
    }

    /**
     * Create a new rectangle
     *
     * @param width The width of the rectangle
     * @param height The height of the rectangle
     * @param x The position x in the gridpane
     * @param y The position y in the gridpane
     * @param game The current instance of the game
     */
    public MyRectangle(double width, double height, int x, int y, Game game) {
        super(width, height);

        color = Color.TRANSPARENT;
        stroke = Color.BLACK;
        this.setStroke(stroke);
        this.x = x;
        this.y = y;
        if (game.getBoard()[x][y].isEmpty()) {
            this.fill();
        } else {
            this.empty();
        }

    }

    /**
     * Fill the rectangle with a transparent color when it's empty
     */
    public final void empty() {
        lastColor = color;
        color = Color.TRANSPARENT;
        this.setFill(color);
    }

    /**
     * Fill the rectangle with the new color
     *
     * @param color The color which one we fill the rectangle
     */
    public final void fillPiece(Color color) {
        lastColor = this.color;
        this.color = color;
        this.setFill(color);
    }

    /**
     * If the rectangle is not empty, we fill it in black
     */
    private void fill() {
        lastColor = this.color;
        this.color = Color.BLACK;
        this.setFill(color);
    }

    /**
     * Refill with the previous color
     */
    public void lastColor() {
        color = lastColor;
        this.setFill(color);
    }

}
