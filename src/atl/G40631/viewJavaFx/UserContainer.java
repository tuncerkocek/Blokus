/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import javafx.scene.layout.VBox;

/**
 *
 * @author tuncer
 */
public class UserContainer extends VBox {

    private final HBoxPlayer playerId;

    /**
     * Create an vbox that will contains all user's information
     *
     * @param game The current instance of game
     * @param j The total number of player
     */
    public UserContainer(Game game, int j) {

        playerId = new HBoxPlayer(game, j);
        LinePiece line1 = new LinePiece(game, j, 0, 7);
        LinePiece line2 = new LinePiece(game, j, 7, 14);
        LinePiece line3 = new LinePiece(game, j, 14, 21);
        this.getChildren().addAll(playerId, line1, line2, line3);
        this.setStyle("-fx-border-width:1px;-fx-border-color:black;");
    }

}
