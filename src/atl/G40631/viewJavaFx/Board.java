/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.viewJavaFx;

import atl.G40631.model.Game;
import atl.G40631.observable.Observable;
import atl.G40631.observable.Observer;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;

/**
 *
 * @author Tuncer
 */
public class Board extends GridPane implements Observer {

    /*
    *Create a piece and add it to the pane
    *@param pane The gridPane where to add the piece
    *@param point The point of the piece.
    *@param x The x position where to place rectangle in GridPane
    *@param y The y position where to place rectangle in GridPane
    *@param negation The substracted number to make a gridpane like a graph
    *@param lignePiece1 One of the HBox. We need that just for the maxWidth.
     */
    private int x;
    private int y;
    private final List<MyRectangle> rectangleList;

    /**
     * Create a new gridpane that serve us like a board
     *
     * @param game The current instance of game
     * @param size The size of the board
     */
    public Board(Game game, double size) {
        game.registerObserver(this);
        this.setAlignment(Pos.BASELINE_CENTER);
        rectangleList = new ArrayList();
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();//taile totale de l'écran
        size = (primaryScreenBounds.getHeight() / size) - 15;
        for (int i = 0; i < game.getBoard().length; i++) {
            for (int j = 0; j < game.getBoard()[1].length; j++) {
                MyRectangle rect = new MyRectangle(size, size, i, j, game);
                Board.setConstraints(rect, i, j);
                getChildren().add(rect);
            }

        }

        this.setStyle("-fx-border:black");
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                game.retournerPiece();
            } else {
                try {
                    if (game.playerSize() != 0 && !game.isIa()) {
                        MyRectangle rect = (MyRectangle) event.getTarget();
                        x = rect.getXX();
                        y = rect.getYY();
                        game.addBoard(x, y);
                    }
                } catch (ClassCastException e) {
                    System.out.println("e" + e.getMessage());
                }
            }
        });

        this.addEventHandler(MouseEvent.MOUSE_MOVED, (MouseEvent event) -> {

            try {
                MyRectangle rect = (MyRectangle) event.getTarget();
                if (game.playerSize() != 0 && !game.isIa()) {
                    game.visual(rect.getXX(), rect.getYY());
                }
            } catch (ClassCastException e) {
                System.out.println("e" + e.getMessage());
            }
        });
        if (game.isIa()) {
            game.nexTourIA();
        }
    }

    @Override
    public void update(Observable obs) {

    }

    private void clearPlateau(Game game) { //attention risque de bug
        rectangleList.forEach((unRectangle) -> {
            this.getChildren().forEach((rectangle) -> {
                try {
                    MyRectangle rectanglee = (MyRectangle) rectangle;

                    if (rectanglee.getXX() == unRectangle.getXX() && rectanglee.getYY() == unRectangle.getYY()) {
                        rectanglee.lastColor();
                    }
                } catch (Exception e) {
                }
            });
        });

        rectangleList.clear();
    }

    private void visual(Game game) {

        this.getChildren().forEach((rectangle) -> {
            try {
                MyRectangle rectanglee = (MyRectangle) rectangle;
                game.getHistorique().getForm().forEach((point) -> {
                    int sumX = (int) game.getMouseX() + game.giveXOfAPoint(point);
                    int sumY = (int) game.getMouseY() + game.giveYOfAPoint(point);
                    if (rectanglee.getXX() == sumX && rectanglee.getYY() == sumY) {
                        rectanglee.fillPiece(game.getColorCurrentPlayer());
                        rectangleList.add(rectanglee);
                    }
                });
            } catch (Exception e) {
            }
        });
    }

    @Override
    public void update(int numero, String playerName) {

    }

    @Override
    public void update(String playerName, Game game) {

        clearPlateau(game);
        switch (playerName) {
            case "Alert":
                alert();
                break;
            case "Visual":
                visual(game);
                break;
            case "Add":
                addBoard(game);
                break;
            default:
                break;
        }
    }

    private void addBoard(Game game) {
        this.getChildren().forEach((rectangle) -> {
            try {
                MyRectangle rectanglee = (MyRectangle) rectangle;
                game.getHistorique().getForm().forEach((point) -> {
                    int sommeX = (int) game.getMouseX() + game.giveXOfAPoint(point);
                    int sommeY = (int) game.getMouseY() + game.giveYOfAPoint(point);
                    if (rectanglee.getXX() == sommeX && rectanglee.getYY() == sommeY) {
                        rectanglee.fillPiece(game.getBoard()[rectanglee.getXX()][rectanglee.getYY()].getCouleur());
                    }
                });
            } catch (Exception e) {
            }
        });

    }

    private void alert() {
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setTitle("Plus de joueur");
        a.setHeaderText("Il n'y a plus de joueur dans la partie.");
        a.setContentText("Il n'y a plus de joueur dans la partie. Veuillez recommencer une nouvelle partie.");
        a.show();
    }

}
