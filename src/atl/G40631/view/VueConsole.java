/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.G40631.view;

import atl.G40631.model.Game;
import atl.G40631.model.PlateauSizeException;
import java.util.Scanner;

/**
 *
 * @author G40631
 */
public class VueConsole {

    /**
     *
     * @param args
     * @throws atl.G40631.model.PlateauSizeException
     */
    public static void main(String[] args) throws PlateauSizeException {
        String reading = " ";
        int boardSize = 20;
        Game game = new Game();

        game.start(boardSize);
        System.out.println("Bonjour, bienvenue sur Blokus \n\nQue voulez-vous faire ?\n\n");
        System.out.println("Show : pour afficher le plateau");
        System.out.println("Stock : pour voir le stock du joueur");
        System.out.println("Play n i j : pour jouer une pièce à une position");
        try (Scanner scan = new Scanner(System.in)) {
            while (!reading.equals("exit")) {
                System.out.println("A vous de jouer " + game.getNom());
                reading = read(scan);
                reading = verifRead(game, reading, boardSize);
            }
        }
    }

    /*
    *Sort and choose what to do
     */
    private static String verifRead(Game game, String reading, int boardSize) {
        if (reading.equals("Show") || reading.contains("show")) {
            System.out.println(plateauString(game, boardSize));
        } else if (reading.equals("Stock") || reading.contains("stock")) {
            System.out.println(stockString(game));
        } else if (reading.contains("Play") || reading.contains("play")) {
            jouer(game, reading);
        } else if (reading.contains("exit") || reading.contains("exit")) {
            reading = "exit";
        }
        return reading;
    }

    /*
    *Execute the play command
     */
    private static void jouer(Game game, String reading) {
        String[] split = reading.split(" ");
        try {
            int n = Integer.parseInt(split[1]) - 1;
            int i = Integer.parseInt(split[2]);
            int j = Integer.parseInt(split[3]);
            if (game.verifStock(n)) {
                if (game.verifPosition(i, j)) {
                    if (!game.playTour(n, i, j)) {
                        System.out.println("Une pièce se trouve déjà à cette position. Insérer la pièce dans une position vide.");
                    } else {
                        game.nextTour(false);
                    }
                } else {
                    System.out.println("Veuillez entrer des coordonnées exactes");
                }

            } else {
                System.out.println("Veuillez mettre un numéro de pièce existant dans votre stock");
            }

        } catch (NumberFormatException e) {
            System.out.println("Erreur de donnée. Veuillez entrez play suivi de 3 entiers.");
        }
    }

    /*
    *Scan the entry
    *@return the scanned entry
     */
    private static String read(Scanner scan) {
        return scan.nextLine();
    }

    /*
    *Show the board
    *@return the board
     */
    private static String plateauString(Game game, int boardSize) {
        StringBuilder board = new StringBuilder();
        int size = boardSize - 1;
        for (int i = 0; i < boardSize; i++) {

            if (i < 10) {

                board.append(" |").append(i).append("");
            } else {
                board.append("|").append(i).append("");
            }
        }
        board.append("|\n");
        for (int i = 0; i < game.getBoard().length; i++) {
            board.append(" |");
            for (int j = 0; j < game.getBoard()[0].length; j++) {
                if (game.getBoard()[j][i + size].isEmpty()) {
                    board.append(" ").append(game.getBoard()[j][i + size].getName().charAt(0)).append("|");
                } else {
                    board.append("  |");
                }
            }

            board.append((i + size)).append("\n");
            size = size - 2;
        }
        return board.toString();
    }

    /*
    *Show the stock
    *@return The stock.
     */
    private static String stockString(Game game) {

        boolean[] stock = game.getStock();
        StringBuilder stocks = new StringBuilder();
        stocks.append(game.getNom()).append(": \n");
        for (int i = 0; i < stock.length; i++) {
            if (stock[i]) {
                stocks.append(i + 1).append(" ");
            }
        }
        return stocks.toString();
    }
}
